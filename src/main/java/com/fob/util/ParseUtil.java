package com.fob.util;

public class ParseUtil {

	public static String parse(String request) {
		String response = new String();
		
		for (int index = 0; index < request.length() ; index ++) {
			try {
				Integer num = Integer.parseInt(request.substring(index, index+1));
				response += num > 2? "F" : "O";
			} catch (Exception e) {
				response += "B";
			}
		}
		
		return response;
	}
}
