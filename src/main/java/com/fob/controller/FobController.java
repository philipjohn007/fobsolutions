package com.fob.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.fob.util.ParseUtil;

@RestController
@RequestMapping("/rest/fob")
public class FobController {

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST)
	public String parseString(@RequestBody String request) {
		return ParseUtil.parse(request);
	}
}
