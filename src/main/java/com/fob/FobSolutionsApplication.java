package com.fob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FobSolutionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FobSolutionsApplication.class, args);
	}
}
