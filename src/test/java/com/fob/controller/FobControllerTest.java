package com.fob.controller;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FobControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Test
    public void testNumberString() {
    	check("123","OOF");
    }
    
    @Test
    public void testMixedString() {
    	check("123AB","OOFBB");
    }
    
    private void check(String req, String expected) {
    	ResponseEntity<String> response = this.restTemplate.postForEntity("/rest/fob", req, String.class);
    	assertTrue(expected.equals(response.getBody()));
    }
}
